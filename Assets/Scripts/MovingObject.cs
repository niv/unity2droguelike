﻿using UnityEngine;
using System.Collections;

public abstract class MovingObject : MonoBehaviour {

    public float moveTime = 0.1f; // time it takes object to move in seconds
    public LayerMask blockingLayer; // for checking if we are allowed to move to a given spot by checking if its in blocking layer

    private BoxCollider2D collider;
    private Rigidbody2D rb;
    private float inverseMoveTime; // for effeciancy computations

    private bool movementDone = true;

	// Use this for initialization
	protected virtual void Start () {
        collider = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        inverseMoveTime = 1f / moveTime;

    }

    protected bool Move(int xDir, int yDir, out RaycastHit2D hit) {
        Vector2 start = transform.position;
        Vector2 end = start + new Vector2(xDir, yDir);

        //Debug.Log("Moving.." + start + "," + end);

        collider.enabled = false; // so we wont hit our own collider
        hit = Physics2D.Linecast(start, end, blockingLayer);
        collider.enabled = true;

        if (!movementDone)
            return false;

        if (hit.transform == null) {
            movementDone = false; //!!!
            StartCoroutine(SmoothMovement(end));
            return true;
        }

        return false;
    }

    protected IEnumerator SmoothMovement(Vector3 end) {
        Debug.Log("START end=" + end);
        float sqrRemainingDistance = (end - transform.position).sqrMagnitude;

        while(sqrRemainingDistance > float.Epsilon) {
            Vector3 newPos = Vector3.MoveTowards(rb.position, end, inverseMoveTime * Time.deltaTime); // after moveTime times of this occuring, we'll get to our "end" destination as returned value
            rb.MovePosition(newPos);
            sqrRemainingDistance = (end - transform.position).sqrMagnitude;
            yield return null;
        }
        movementDone = true;
        Debug.Log("END end=" + end);
    }

    /// <summary>
    /// Attempt to move to a given (x,y)
    /// Excute OnCantMove if path blocked by object of type T
    /// </summary>
    /// <typeparam name="T">type of objects we want to interact with if collided</typeparam>
    protected virtual void AttemptMove<T>(int xDir, int yDir)
        where T : Component {
        RaycastHit2D hit;
        bool canMove = Move(xDir, yDir, out hit);
        
        if (hit.transform == null)
            return;

        T hitComponent = hit.transform.GetComponent<T>();

        if (!canMove && hitComponent != null)
            OnCantMove(hitComponent);
        
    }

    protected abstract void OnCantMove<T>(T component)
        where T : Component;
}
