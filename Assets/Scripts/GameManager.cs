﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

    public float levelStartDelay = 1.5f;
    public float turnDelay = .1f; // wait between turns
    public static GameManager instance = null;
    public BoardManager boardScript;
    public int playerFoodPoints = 100;
    [HideInInspector] public bool playerTurn = true;


    private Text levelText;
    private GameObject levelImage;
    private bool doingSetup;

    private int level = 1;
    private List<Enemy> enemies;
    private bool enemiesMoving;


	// Use this for initialization
	void Awake () {
        if (instance == null)
            instance = this;
        else if (instance != this) {
            Destroy(gameObject);
            Debug.Log("GameManager extra destroyed");
        }
        DontDestroyOnLoad(gameObject); // wont destroy this on new scene
        boardScript = GetComponent<BoardManager>();
        enemies = new List<Enemy>();
        InitGame();
	}

    // called every time scene is loaded
    private void OnLevelWasLoaded(int index) {
        level++;
        InitGame();
    }

    void InitGame() {
        doingSetup = true;

        levelImage = GameObject.Find("LevelImage");
        levelText = GameObject.Find("LevelText").GetComponent<Text>();
        levelText.text = "Day " + level;
        levelImage.SetActive(true);
        Invoke("HideLevelImage", levelStartDelay);

        enemies.Clear();
        boardScript.SetupScene(level);
    }
	

    private void HideLevelImage() {
        levelImage.SetActive(false);
        doingSetup = false;
    }

    public void GameOver() {
        levelText.text = "After " + level + " days, you starved.";
        levelImage.SetActive(true);
        enabled = false;
    }

    void Update() {
        if (playerTurn || enemiesMoving || doingSetup)
            return;

        StartCoroutine(MoveEnemies());
    }

    public void AddEnemyToList(Enemy script) {
        enemies.Add(script);
    }

	IEnumerator MoveEnemies() {
        enemiesMoving = true;
        yield return new WaitForSeconds(turnDelay);
        if (enemies.Count == 0) {
            yield return new WaitForSeconds(turnDelay);
        }

        for(int i = 0; i < enemies.Count; i++) {
            enemies[i].MoveEnemy();
            yield return new WaitForSeconds(enemies[i].moveTime);
        }

        playerTurn = true;
        enemiesMoving = false;
    }
}
