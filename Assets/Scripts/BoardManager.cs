﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Random = UnityEngine.Random;

public class BoardManager : MonoBehaviour {

    [Serializable]
    public class Count {
        public int min, max;
        public Count(int min, int max) { this.min = min; this.max = max; }
    }

    /************************************************************************************************/
    
    public int columns = 8;
    public int rows = 8;
    public Count wallCount = new Count(5, 9);
    public Count foodCount = new Count(1, 4);

    public GameObject exit;
    public GameObject[] floorTiles;
    public GameObject[] wallTiles;
    public GameObject[] outerwallTiles;
    public GameObject[] foodTiles;
    public GameObject[] enemyTiles;

    private Transform boardHolder;
    private List<Vector3> gridPositions = new List<Vector3>();

    /************************************************************************************************/


    void InitialiseList() {
        gridPositions.Clear();

        for(int x = 0; x < columns - 1; x++) {
            for(int y = 0; y < rows - 1; y++) {
                gridPositions.Add(new Vector3(x, y, 0f));
            }
        }
    }


    // return and remove a random position from the gridPositions
    Vector3 ExtractRandomPosition() {
        int randIndex = Random.Range(0, gridPositions.Count);
        Vector3 randPosition = gridPositions[randIndex];
        gridPositions.RemoveAt(randIndex);
        return randPosition;
    }


    GameObject GetRandomWall(bool is_outer) {
        if (is_outer) return outerwallTiles[Random.Range(0, outerwallTiles.Length)];
        else return floorTiles[Random.Range(0, floorTiles.Length)];
    }

    void BoardSetup() {
        boardHolder = new GameObject("Board").transform;

        for (int x = -1; x < columns + 1; x++) {
            for (int y = -1; y < rows +1; y++) {
                GameObject toInstantiate = GetRandomWall(x == -1 || y == -1 || x == columns || y == columns);
                GameObject instance = Instantiate(toInstantiate, new Vector3(x, y, 0f), Quaternion.identity) as GameObject;
                instance.transform.SetParent(boardHolder);
            }
        }
    }

    void LayoutObjectsRandomly(GameObject[] tiles, int min, int max) {
        int objCount = Random.Range(min, max + 1);

        for(int i = 0; i < objCount; i++) {
            Vector3 randPos = ExtractRandomPosition();
            GameObject randTile = tiles[Random.Range(0, tiles.Length)];
            GameObject new_obj = Instantiate(randTile, randPos, Quaternion.identity) as GameObject;
            //new_obj.transform.SetParent(boardHolder);
        }
    }


    public void SetupScene(int level) {
        BoardSetup();
        InitialiseList();
        gridPositions.RemoveAt(0); // for player to appear on (0,0) we remove the first index of this list, so others wont appear there
        LayoutObjectsRandomly(wallTiles, wallCount.min, wallCount.max);
        LayoutObjectsRandomly(foodTiles, foodCount.min, foodCount.max);
        int enemyCount = (int)Mathf.Log(level, 2f);
        LayoutObjectsRandomly(enemyTiles, enemyCount, enemyCount);
        Instantiate(exit, new Vector3(columns - 1, rows - 1, 0), Quaternion.identity);
    }
    
}
