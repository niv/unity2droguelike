﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

public class Player : MovingObject {

    public int wallDamage = 1;
    public int pointsPerFood = 10;
    public int pointsPerSoda = 20;
    public float restartLevelDelay = 1f;
    public Text foodText;


    private Animator animator;
    private int food;


    protected override void OnCantMove<T>(T component) {
        Wall hitWall = component as Wall;
        hitWall.DamageWall(wallDamage);
        animator.SetTrigger("playerChop");
    }


    // Use this for initialization
    protected override void Start () {
        animator = GetComponent<Animator>();

        food = GameManager.instance.playerFoodPoints; // storing it as we change levels

        foodText.text = "Food: " + food;

        base.Start();
	
	}
    
    private void OnDisable() {
        GameManager.instance.playerFoodPoints = food;
    }


    // Update is called once per frame
    void Update () {
        if (!GameManager.instance.playerTurn) return;
        //Debug.Log("in update");
        int horizontal = 0;
        int vertical = 0;

        horizontal = (int)Input.GetAxisRaw("Horizontal");
        vertical = (int)Input.GetAxisRaw("Vertical");

        if (horizontal != 0)
            vertical = 0;

        if (horizontal != 0 || vertical != 0) {
            AttemptMove<Wall>(horizontal, vertical);
        }
        //Debug.Log("done update");
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.tag == "Exit") {
            Invoke("Restart", restartLevelDelay);
            enabled = false;
        }
        else if (other.tag == "Food") {
            food += pointsPerFood;
            foodText.text = "+" + pointsPerFood + " Food: " + food;
            other.gameObject.SetActive(false);
        }
        else if(other.tag=="Soda") {
            food += pointsPerSoda;
            foodText.text = "+" + pointsPerSoda + " Food: " + food;
            other.gameObject.SetActive(false);
        }
    }

    public void LoseFood(int loss) {
        animator.SetTrigger("playerHit");
        food -= loss;
        foodText.text = "-" + loss + " Food: " + food;
        CheckIfGameOver();
    }


    protected override void AttemptMove<T>(int x, int y) {
        food--;
        foodText.text = "Food: " + food;

        ///////GameManager.instance.playerTurn = false;
        base.AttemptMove<T>(x, y);

        RaycastHit2D hit;

        CheckIfGameOver();

        GameManager.instance.playerTurn = false;
    }

    private void Restart() {
        Application.LoadLevel(Application.loadedLevel); // loading last scene
        
    }


    private void CheckIfGameOver() {
        if (food <= 0) {
            GameManager.instance.GameOver();
        }
    }
}
