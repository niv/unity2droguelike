﻿using UnityEngine;
using System.Collections;
using System;

public class Enemy : MovingObject {

    public int playerDamage;


    private Animator animator;
    private Transform target;
    private bool skipMove;

	// Use this for initialization
	protected override void Start () {
        animator = GetComponent<Animator>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        GameManager.instance.AddEnemyToList(this);
        base.Start();
	}
	

    protected override void AttemptMove<T>(int xDir, int yDir) {

        if(skipMove) { // only move every-other turn
            skipMove = false;
            return;
        }

        base.AttemptMove<T>(xDir, yDir);

        skipMove = true;
    }

    public void MoveEnemy() {
        int x = 0, y = 0;

        if(Mathf.Abs(target.position.x - transform.position.x) < float.Epsilon) {
            y = target.position.y > transform.position.y ? +1 : -1;
        } else {
            x = target.position.x > transform.position.x ? +1 : -1;
        }

        AttemptMove<Player>(x, y);
    }
    
    protected override void OnCantMove<T>(T component) {
        Player hitPlayer = component as Player;
        animator.SetTrigger("enemyAttack");
        hitPlayer.LoseFood(playerDamage);
    }
}
